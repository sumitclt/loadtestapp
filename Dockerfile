FROM openjdk:8
MAINTAINER "Brighty Sara Babu" <brighty.babu@cleartrip.com>
WORKDIR /opt/app
ENV NLS_LANG="AMERICAN_AMERICA.UTF8"
ENV LC_ALL="en_US.UTF-8"
RUN mkdir -p /opt/app/logs
RUN mkdir /opt/newrelic/
COPY newrelic/newrelic.jar /opt/newrelic/
COPY newrelic/newrelic.yml /opt/newrelic/
ADD  build/libs/load-test-app-0.0.1-SNAPSHOT.jar /opt/app
EXPOSE 9080
CMD ["/usr/bin/java", "-javaagent:/opt/newrelic/newrelic.jar","-jar", "/opt/app/load-test-app-0.0.1-SNAPSHOT.jar"]