package com.cleartrip.common.loadtestapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LoadTestAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(LoadTestAppApplication.class, args);
	}

}
