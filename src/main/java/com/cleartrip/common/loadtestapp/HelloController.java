package com.cleartrip.common.loadtestapp;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

import java.time.Duration;

@RestController
public class HelloController {

    @RequestMapping("/hello")
    public Mono<String> sayHello() {
        long delay = (long)(Math.random()*300);
        return Mono.delay(Duration.ofMillis(delay)).map(d -> "Hi." + delay);
    }

    @RequestMapping("/helloWithDelay")
    public Mono<String> sayHelloWithDelay(@RequestParam("delay") long delay) {
        return Mono.delay(Duration.ofMillis(delay)).map(d -> "Hi." + delay);
    }

    @RequestMapping("/ping")
    public String ping() {
        return "PONG";
    }

}
